module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    
    // uglify: {
    //   options: {
    //     banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
    //   },
    //   build: {
    //     files: {
    //       'js/build/out.min.js': 'js/src/*.js'
    //     }
    //   }
    // },

    concat: {
      build: {
        src: ['js/src/**/*.js'],
        dest: 'js/build/<%= pkg.name %>.js'
      }
    },

    jshint: {
      files: ['Gruntfile.js', 'js/src/*.js'],
    },

    qunit: {

    },

    watch: {
      scripts: {
        files: ['<%= jshint.files %>'],
        tasks: ['jshint']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['concat', 'jshint']);
};