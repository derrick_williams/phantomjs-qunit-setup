(function(root) {
  var Messenger;

  root.Messenger = Messenger = function() {
    var self = this,
        callbacks = [];

    self.send = function(msg) {
      console.log(msg);
    }; 

    self.receive = function(callback) {
      callbacks.push(callback);
      console.log('you added a receive callback');
    };

  };

})(window);